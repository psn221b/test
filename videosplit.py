from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
from moviepy.editor import VideoFileClip


def video_split(time,filename):
	clip = VideoFileClip(filename)
	ffmpeg_extract_subclip(filename,float(0),float(time[0])/100,targetname="cut"+str(0)+".mp4")
	for i in range(len(time)-1):
		ffmpeg_extract_subclip(filename,float(time[i])/100,float(time[i+1])/100,targetname="cut"+str(i+1)+".mp4")
	ffmpeg_extract_subclip(filename,float(time[len(time)-1])/100, clip.duration,targetname="cut"+str(len(time))+".mp4")
	clip.close()